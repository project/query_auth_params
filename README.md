CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Recommended modules
* Configuration
* Troubleshooting
* Maintainers

INTRODUCTION
------------

 Provides common and resuable token UI elements and missing core tokens.

* For a full description of the module, visit the project page:
   <https://drupal.org/project/query_auth_params>

* To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/query_auth_params>

RECOMMENDED MODULES
-------------------

* No extra module is required.

INSTALLATION
------------

* Install as usual, see
   <https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8> for further
   information.

CONFIGURATION
-------------

* For adding pages that you want to protect
* go to /admin/config/development/query_auth_params and add the page(s) you want to protect along with the query params.



MAINTAINERS
-----------

Current maintainers:

* Nikitas Michalakis (<https://drupal.org/user/410290>)
