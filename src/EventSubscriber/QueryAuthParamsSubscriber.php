<?php

namespace Drupal\query_auth_params\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Query auth params event subscriber.
 */
class QueryAuthParamsSubscriber implements EventSubscriberInterface {


  /**
   * Config settings.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Current User.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * QueryAuthParamsSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Settings.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The currentUser.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $currentUser, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->currentUser = $currentUser;
    $this->messenger = $messenger;
  }

  /**
   * Kernel response event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ControllerEvent $event
   *   Response event.
   */
  public function onKernelController(ControllerEvent $event) {

    $config_settings = $this->configFactory->getEditable('query_auth_params.settings');
    $protect_pages = $config_settings->get('protect_pages');

    if (count($protect_pages) === 0) {
      return;
    }

    $request = $event->getRequest();
    $path = $request->getPathInfo();
    $restricted_paths = array_column($protect_pages, 'restrict_url');
    $protect_page_exists = array_search($path, $restricted_paths);

    if ($protect_page_exists === FALSE) {
      return;
    }

    $redirect = new RedirectResponse('/system/403');
    $params = $request->query->all();

    // NO params were provided, Exit.
    if (empty($params)) {
      $redirect->send();
    }

    // Get the current protect page.
    $protect_page = $protect_pages[$protect_page_exists];

    // Is this is a once time view?
    if (!isset($protect_page['settings']) || !isset($protect_page['shown']) || !isset($protect_page['param_name']) || !isset($protect_page['param_value'])) {
      $redirect->send();
    }

    // The page was previously displayed.
    if ($protect_page['settings'] === 'once' && $protect_page['shown'] === 1) {
      $redirect->send();
    }

    $param_name = $protect_page['param_name'];
    $param_value = $protect_page['param_value'];

    // Params are wrong.
    if (!isset($params[$param_name]) || $params[$param_name] !== $param_value) {
      $redirect->send();
    }

    // Params are set properly.
    if ($params[$param_name] === $param_value) {

      if ($protect_page['settings'] === 'once') {
        $protect_pages[$protect_page_exists]['shown'] = 1;
        $config_settings->set('protect_pages', $protect_pages)->save();
      }

      if ($protect_page['settings'] === 'datetime_period') {
        $show_until = $protect_page['datetime_period'];
        $current_datetime = time();

        if ($current_datetime > $show_until) {
          $redirect->send();
        }
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::CONTROLLER => ['onKernelController'],
    ];
  }

}
