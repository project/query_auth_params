<?php

namespace Drupal\query_auth_params\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure query auth params settings for this site.
 */
class QueryAuthParamsSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'query_auth_params.settings';


  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;


  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'query_auth_params_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The path alias manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AliasManagerInterface $alias_manager, PathValidatorInterface $path_validator, RequestContext $request_context) {
    parent::__construct($config_factory);
    $this->aliasManager = $alias_manager;
    $this->pathValidator = $path_validator;
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('config.factory'),
    $container->get('path_alias.manager'),
    $container->get('path.validator'),
    $container->get('router.request_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get stored settings.
    $config = $this->config(static::SETTINGS);
    $protect_pages = $config->get('protect_pages');
    $total_pages = isset($protect_pages) && is_array($protect_pages) ? count($protect_pages) : 0;
    $alphanumeric_validation_msg = $this->t('Please use only letters and numbers (a-z, A-Z, 0-9). No spaces or symbols are allowed.');
    // Get the number of names in the form already.
    $num_lines = $form_state->get('num_lines');

    // We have to ensure that there is at least one name field.
    if ($num_lines === NULL || !$form_state->isSubmitted()) {
      $form_state->set('num_lines', 1);
      $num_lines = $form_state->get('num_lines');
    }

    if (!$form_state->isSubmitted() && $total_pages > 0) {
      $form_state->set('num_lines', $total_pages);
      $num_lines = $total_pages;
    }

    // Get a list of fields that were removed.
    $removed_fields = $form_state->get('removed_fields');
    // If no fields have been removed yet we use an empty array.
    if ($removed_fields === NULL) {
      $form_state->set('removed_fields', []);
      $removed_fields = $form_state->get('removed_fields');
    }

    $form['#tree'] = TRUE;
    $form['protect_pages'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Protect Pages with Query Auth Params'),
      '#prefix' => '<div id="protect-pages-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_lines; $i++) {

      // Check if field was removed.
      if (in_array($i, $removed_fields)) {
        // Skip if field was removed and move to the next field.
        continue;
      }

      // Fieldset title.
      $form['protect_pages'][$i] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Protect Page') . ' ' . ($i + 1),
      ];

      $form['protect_pages'][$i]['restrict_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Url to apply the Query Auth Params'),
        '#default_value' => isset($protect_pages[$i]) ? $protect_pages[$i]['restrict_url'] : NULL,
        '#size' => 40,
        '#description' => $this->t('Specify a relative URL to apply the query auth params filter.'),
        '#field_prefix' => $this->requestContext->getCompleteBaseUrl(),
        '#required' => TRUE,
      ];

      $form['protect_pages'][$i]['param_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Query param name'),
        '#default_value' => $protect_pages[$i]['param_name'] ?? NULL,
        '#required' => TRUE,
        '#maxlength' => 10,
        '#pattern' => '[a-zA-Z0-9]+',
        '#description' => $alphanumeric_validation_msg,
      ];

      $form['protect_pages'][$i]['param_value'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Query param value'),
        '#default_value' => $protect_pages[$i]['param_value'] ?? NULL,
        '#required' => TRUE,
        '#maxlength' => 10,
        '#pattern' => '[a-zA-Z0-9]+',
        '#description' => $alphanumeric_validation_msg,
      ];

      // Radios.
      $form['protect_pages'][$i]['settings'] = [
        '#type' => 'radios',
        '#title' => $this->t('Display Options'),
        '#required' => TRUE,
        '#options' => [
          'forever' => $this->t('Forever'),
          'once' => $this->t('Once'),
          'datetime_period' => $this->t('Upto Datetime'),
        ],
        '#description' => $this->t('Please select the criteria for displaying this page.'),
        '#default_value' => $protect_pages[$i]['settings'] ?? 'forever',
      ];

      // Date-time.
      $default_datetime = DrupalDateTime::createFromTimestamp(time());
      if (isset($protect_pages[$i]['datetime_period'])) {
        $default_datetime = DrupalDateTime::createFromTimestamp($protect_pages[$i]['datetime_period']);
      }

      $form['protect_pages'][$i]['container'] = [
        '#type' => 'container',
        '#states' => [
          'required' => [
            ':input[name="protect_pages[' . $i . '][settings]"]' => ['value' => 'datetime_period'],
          ],
          'visible' => [
            ':input[name="protect_pages[' . $i . '][settings]"]' => ['value' => 'datetime_period'],
          ],
        ],
      ];
      $form['protect_pages'][$i]['container']['datetime_period'] = [
        '#type' => 'datetime',
        '#title' => 'Date Time',
        '#date_increment' => 1,
        '#date_timezone' => date_default_timezone_get(),
        '#default_value' => $default_datetime,
        '#description' => $this->t('Date time, #type = datetime'),
      ];

      $form['protect_pages'][$i]['shown'] = [
        '#type' => 'hidden',
        '#value' => 0,
      ];

      $form['protect_pages'][$i]['actions'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => $i,
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'protect-pages-fieldset-wrapper',
        ],
      ];

    }

    $form['protect_pages']['actions'] = [
      '#type' => 'actions',
    ];

    $form['protect_pages']['actions']['add_protect_page'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'protect-pages-fieldset-wrapper',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['protect_pages'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $num_field = $form_state->get('num_lines');
    $add_button = $num_field + 1;
    $form_state->set('num_lines', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove" button.
   *
   * Removes the corresponding line.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    /*
     * We use the name of the remove button to find
     * the element we want to remove
     */
    $trigger = $form_state->getTriggeringElement();
    $indexToRemove = $trigger['#name'];

    // Remove the fieldset from $form (the easy way)
    unset($form['protect_pages'][$indexToRemove]);

    // Remove the fieldset from $form_state (the hard way)
    // First fetch the fieldset, then edit it, then set it again
    // Form API does not allow us to directly edit the field.
    $namesFieldset = $form_state->getValue('protect_pages');
    unset($namesFieldset[$indexToRemove]);
    $form_state->setValue('protect_pages', $namesFieldset);

    // Keep track of removed fields so we can add new fields at the bottom
    // Without this they would be added where a value was removed.
    $removed_fields = $form_state->get('removed_fields');
    $removed_fields[] = $indexToRemove;
    $form_state->set('removed_fields', $removed_fields);

    // Rebuild form_state.
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $alphanumeric_validation_msg = $this->t('Please use only letters and numbers (a-z, A-Z, 0-9). No spaces or symbols are allowed.');
    $protect_pages = $form_state->getValue('protect_pages');
    $paths = [];
    foreach ($protect_pages as $key => $protect_page) {

      if ($key === 'actions') {
        continue;
      }

      // Make sure protected page path is unique.
      if (in_array($protect_page['restrict_url'], $paths)) {
        $form_state->setErrorByName("protect_pages][$key][restrict_url", $this->t("The path '%path' has been added already.", ['%path' => $protect_page['restrict_url']]));
      }
      else {
        $paths[] = $protect_page['restrict_url'];
      }

      // Validate protected page path.
      if (!empty($protect_page['restrict_url']) && $protect_page['restrict_url'][0] !== '/') {
        $form_state->setErrorByName("protect_pages][$key][restrict_url", $this->t("The path '%path' has to start with a slash.", ['%path' => $protect_page['restrict_url']]));
      }

      if (!$this->pathValidator->isValid($protect_page['restrict_url'])) {
        $form_state->setErrorByName("protect_pages][$key][restrict_url", $this->t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $protect_page['restrict_url']]));
      }

      // Param name is alphanumeric.
      if (!ctype_alnum($protect_page['param_name'])) {
        $form_state->setErrorByName("protect_pages][$key][param_name", $alphanumeric_validation_msg);
      }

      // Param value is alphanumeric.
      if (!ctype_alnum($protect_page['param_value'])) {
        $form_state->setErrorByName("protect_pages][$key][param_value", $alphanumeric_validation_msg);
      }

      // Date is not set.
      if (empty($protect_page['container']['datetime_period'])) {
        $form_state->setErrorByName("protect_pages][$key][container][datetime_period", $this->t('Please enter a datetime or select another display option.'));
      }

      parent::validateForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $protect_pages = [];
    $protect_pages_values = $form_state->getValue('protect_pages');

    if (isset($protect_pages_values['actions'])) {
      unset($protect_pages_values['actions']);
    }

    if (isset($protect_pages_values) & is_array($protect_pages_values) && count($protect_pages_values) > 0) {

      foreach ($protect_pages_values as $page_values) {

        if (isset($page_values['container']['datetime_period'])) {
          $page_values['datetime_period'] = $page_values['container']['datetime_period']->getTimestamp();
          unset($page_values['container']);
        }

        $protect_pages[] = $page_values;
      }
    }
    $this->config(static::SETTINGS)->set('protect_pages', $protect_pages)->save();
    $this->messenger()->addMessage($this->t('Query Auth Params have been updated!'));
    parent::submitForm($form, $form_state);
  }

}
